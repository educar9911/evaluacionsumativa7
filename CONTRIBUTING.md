from flask import Flask
from flask import Resource, Api

app = Flask(__name__)
api = Api(app)

@app.route('/')
class slang(Resource):
    def get(self):
        return {
            'Slang Panameño': {
                'Palabras': ['Xopa','Mopri','Chombo','Pelaita','Chiquillo','Man']
        },
		  'Descripciones': {
				'Significados': ['que paso','Amigo o pasiero','Moreno o negro','Niña de corta edad','Niño Pequeño','Hombre']
			}


}

api.add_resource(slang, '/')

if __name__ == '__main__':
    app.run(host='127.0.0.1', port=8000, debug=True)

